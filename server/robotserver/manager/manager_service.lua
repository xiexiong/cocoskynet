--
-- Author:      name
-- DateTime:    2018-04-23 17:19:33
-- Description: 服务管理


require "skynet.manager"
local skynet = require "skynet"
local cluster = require "skynet.cluster"

local NodeMessage = require "NodeMessage"
local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "MessageHandler"
local ClusterClient = require "ClusterClient"



local function init()
	local node_message = NodeMessage.new()
	local message_dispatch = MessageDispatch.new()	
	local message_handler = MessageHandler.new(message_dispatch)
	local cluster_client = ClusterClient.new(message_dispatch, node_message)
	--开启集群节点
	cluster_client:openCluster()
	--lua消息的派发
	skynet.dispatch("lua", message_dispatch:dispatch())		
end


---------------------------------------------------------
-- skynet
---------------------------------------------------------

skynet.start(function()

	init()
	skynet.register('.proxy')
	
end)