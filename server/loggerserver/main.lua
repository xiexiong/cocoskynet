--
-- @Author:      name
-- @DateTime:    2018-04-20 21:48:12
-- @Description: 服务器节点配置读取

local skynet = require "skynet"


skynet.start(function()
	--主服务
	local manager = skynet.uniqueservice("manager_service")
	skynet.call(manager, "lua", "start")
	
    skynet.exit()
end)

