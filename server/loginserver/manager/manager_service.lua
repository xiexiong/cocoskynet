--
-- Author:      name
-- DateTime:    2018-04-23 17:19:33
-- Description: 服务管理


require "skynet.manager"
local skynet = require "skynet"
local NodeMessage = require "NodeMessage"
local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "MessageHandler"
local ClientHandler = require "ClientHandler"
local ClusterClient = require "ClusterClient"
local ServerManager = require "manager.ServerManager"

global = {}

local function init()

	message_dispatch = MessageDispatch.new()	
	node_message = NodeMessage.new()
	message_handler = MessageHandler.new(message_dispatch, node_message)
	client_handler = ClientHandler.new(message_dispatch, node_message)
	cluster_client = ClusterClient.new(message_dispatch, node_message)
	server_manager = ServerManager.new()

	global.server_manager = server_manager
	--开启集群节点
	cluster_client:openCluster()		
	skynet.dispatch("lua", message_dispatch:dispatch())		

end
---------------------------------------------------------
-- skynet
---------------------------------------------------------

skynet.start(function()
	init()		
	skynet.register('.proxy')

end)