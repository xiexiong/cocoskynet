--
-- Author:      name
-- DateTime:    2018-05-05 15:46:15
-- Description: 牛牛服务
local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local queue = require "skynet.queue"


local MessageDispatch = require "MessageDispatch"
local UserManager = require "UserManager"
local NodeMessage = require "NodeMessage"
local RoomConfig = require "RoomConfig"
local DeskMessage = require "DeskMessage"

local MessageHandler = require("niuniu.MessageHandler")
local Desk = require("niuniu.Desk")
local Rule = require("niuniu.Rule")



--服务唯一全局变量
global = {}

local queueFunc = queue()


---------------------------------------------------------
-- CMD
---------------------------------------------------------


local function init()
	local node_message = NodeMessage.new()
	local message_dispatch = MessageDispatch.new()
	local rule = Rule.new()
	local desk = Desk.new()
	local desk_message = DeskMessage.new(message_dispatch)
	local message_handler = MessageHandler.new(message_dispatch)
	local user_manager = UserManager.new()
	local room_config = RoomConfig.new() --房间配置

	global.rule = rule
	global.desk = desk
	global.node_message = node_message
	global.user_manager = user_manager
	global.room_config = room_config
	global.desk_message = desk_message
	global.desk_config = nil


	local f = message_dispatch:dispatch()
	function _queue( session, source, cmd, ...)		
		local resut = queueFunc(f,session, source, cmd, ...)
		return result
	end	
	skynet.dispatch("lua",_queue)

end

---------------------------------------------------------
-- skynet
---------------------------------------------------------
skynet.start(function()
	init()

end)