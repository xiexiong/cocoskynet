--
-- Author:      name
-- DateTime:    2018-08-21 15:57:48
-- Description: 游戏主逻辑

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local Timer = require "Timer"
local FSM = require "FSM"
-- local NodeMessage = require "NodeMessage"
-- local Universal = require "Universal"
local cjson = require "cjson"
-- require "common"


local mix_array = mix_array
local table_clone = table_clone
local floor = math.floor
local four_five = math.four_five
local random = math.random


local DeskBase = require "DeskBase"
local Desk = class("Desk",DeskBase)

function Desk:ctor()
	self.super:ctor()
	--https://blog.csdn.net/bigcat133/article/details/46374357
	--极度稀疏数组不输出nil
	cjson.encode_sparse_array(true)
	
end

----------------------------------------------------------------------------------------------
---------开始游戏
---------------------------------------------------------------------------------------------- 
function Desk:start()
	print("___开始游戏___")
	self.node_message = global.node_message	
	self.room_config = global.room_config
	self.user_manager = global.user_manager
	self.rule = global.rule
	self.desk_config = global.desk_config

	self.desk_id = self.desk_config.desk_id
	self.timer = Timer.new()

	
	--配置信息
	self.tbPlayerCard = {} --玩家手牌
	self.tbPlayerBet = {} --闲玩家下注列表
	self.roomCfg = {{settlementMoneyCode = 22},--初级游戏结算
					{settlementMoneyCode = 32}, --中级游戏结算
					{settlementMoneyCode = 42}}--高级游戏结算
	-- local sql = string.format("select * from common_room where sub_id = %d", self.roomid)
	-- local res = skynet.call("mysql_service", "lua", "execute", sql)
	
	-- self.room_config.room_rule_config = cjson.decode(self.room_config.game_config)		
	self.bottom_chip = 1 --底注
	-- self.severFeeRate = self.room_config.room_fee/100	--服务费率
	self.max_seat_num = self.room_config:getSeatCount() --最大椅子数
	self.min_player_num = 2 --最少在桌人数
	self.playing_num = 0 --当前在玩人数
	self.tbuser_id = {}

	--开始时系统坐庄	
	self.trdid = 0	--当前牌桌玩了几局
	self.kickDoNothingNum = 2	--玩家几局未操作踢人

	--状态时间
	self.stateTime = {	
		rest = 5,		--休息时间
		wait = 3,		--等待
		start = 4,		--开始
		bank = 5,		--抢庄
		bankok = 1,		--抢庄
		bet = 5,		--开始(下注)
		leftcard = 5,	--剩余牌
		settlement = 8, --结算时间
	}
	--游戏状态
	--休息，开始玩家操作，抢庄, 下注，发牌，结算
	--state1 --eventName --state2 --callback
	self.stateTable = {
		{"rest", 		"to_start", 			"start", 			function() self:onStart() end },
		{"rest", 		"to_wait", 				"wait", 			function() self:onWait() end },
		{"wait", 		"to_start", 			"start", 			function() self:onStart() end },
		{"start", 		"to_bank", 				"bank", 			function() self:onBank() end },
		{"bank", 		"to_bank_ok", 			"bank_ok", 			function() self:onBankOK() end },
		{"bank_ok", 	"to_bet",				"bet", 				function() self:onBet() end },
		{"bet", 	    "to_leftcard",			"leftcard", 		function() self:onLeftCard() end },
		{"leftcard", 	"to_settlement",		"settlement", 		function() self:onSettlement() end },
		{"settlement", 	"to_rest", 				"rest", 			function() self:onRest() end },
	}
	--状态机
	self.fsm = FSM.new(self.stateTable)
	self:reset()

	--启动状态机
	self.fsm:set("rest")
	self.fsm:fire("to_wait")
end

-------------------------------------------------------------------------------
-- private
-------------------------------------------------------------------------------


--重置数据
function Desk:reset()
	self.rule:shuffle()
	self.tbRaceBankRate = {}
	self.tbPlayerCard = {}
	self.tbPlayerBet = {}
	self.record = {}
	self.bankData = {}
	self.operStartTime = os.time()
	self.player_list = {}
	self.playing_num = 0
	self.tbuser_id = {}

	--设置在线玩家状态
	self.user_manager:setAllUserValue("playing", false)
end

--把不符合条件的玩家踢走
function Desk:checkUser()
	for k,v in pairs(self.player_list) do 
		print("----------checkUser ", v.chip)
		if not v.ready or v.chip < self.minChip
		 or (v.doNothingNum and v.doNothingNum >= self.kickDoNothingNum)
		  then 	
		  local sql = string.format("delete from user_online where uid = %d;", k)
		  skynet.send("mysql_service", "lua", "execute", sql)
			if  v.chip < self.minChip then 
				local data = {nt_kick_table = {
					user_id = k,
					content = '金币不足请离场！',
					kick_reason = 1
				}}
				self:sendMessage(k,data)
			end			

			if v.doNothingNum and v.doNothingNum >= self.kickDoNothingNum then
				local data = {nt_kick_table = {
					user_id = k,
					content = '您已长时间未操作，将自动退出游戏。',
					kick_reason = 2,
				}}
				self:sendMessage(k,data)
			end
			--设置玩家所在桌子id为0	
			self:sendGate(v, 'update_desk_id', 0)
			self:sitUp(k)
			self.player_list[k] = nil
			self:updatePlayerNum()
			print("拼十踢走掉线 or 没准备好 or 筹码不够 or 太久没有操作#######",k)
		end
	end
end

--更新大厅中传来的玩家数据
--updateUserInfo
--disconnect
--getPlayerInfo
--checkAddRobot
--checkRemoveRobot
function Desk:enterDeskCallback(user_id)
	--坐下
	--检查游戏状态
	--是否要加入机器人

end

--离开桌子
function Desk:leaveDeskCallback(user_id)
	--站起
	--从user_manager中删除
	--发送桌子信息变化到zoneserver
end
function Desk:disconnectCallback(user_id)
	--如果不在游戏中
		--站起
		--从user_manager中删除	
end
function Desk:deskStatusCallback(user_id)
	local desk_id = self.desk_id
	local state = self:getStateNum()
	local time = self:getOperTime()
	local config = ""
	local extend = ""
	local users = {}
	local user_list = self.user_manager:getUserList()
	for k,v in pairs(user_list) do 
		print("_______user__________",v)
		local user = {}
		user.user_id = v:getUserId()
		user.nickname = v:getNickname()
		user.seat_id = v:getSeatId()
		user.chip = v:getGold()
		user.icon = ""
		user.diamond = 0
		user.vip_level = 0
		user.level = 0
		user.sex = 0
		user.playing = v:getPlaying()
		user.cards = self:getPlayerCard()	
		table.insert(users, user)
	end
	local extend = self:getDeskStatus()
	local res = {deskStatusRes = {
		desk_id = desk_id,
		state = state,
		time = time,
		users = users,
		extend = extend,		
	}}

	return res
end

--抢庄
function Desk:setBanker(user_id, rate)
	
	if self:getState() ~= "bank" then
		print("_________不在抢庄状态_____")
		return false
	end
	local user = self.user_manager:getUser(user_id)
	self.tbRaceBankRate[user_id] = rate
	local data = {nnUserBankNt={
		desk_id=self.desk_id, 
		user_id=user_id, 
		seat_id=user:getSeatId(),
		rate=rate,
	}}
	self:sendToAllUser(data)

	local raceBankNum = 0 
    local playingNum = 0
    for k,v in pairs(self.player_list) do
		playingNum = playingNum + 1
		if self.tbRaceBankRate[k] then
			raceBankNum = raceBankNum + 1				
		end
    end
    if playingNum>0 and raceBankNum==playingNum then
    	--所有人已抢庄
    	local callback = function ()
			self.fsm:fire("to_bank_ok")
		end
		self:setTimer(1, callback)
    end
    return true
end	


--请求下注
function Desk:userBet(user_id, rate)
	local user = self.user_manager:getUser(user_id)
	if self:getState()~="bet" then
		print("_________不在下注状态_____")
		return false
	end
	if rate>self:getMaxPlayerRate() then
		print("_________超过最高下注倍数_____")
		return false
	end	
	if self.bankData.user_id==user_id then
		print("_________庄不能下注_____")
		return false
	end
	if self.tbPlayerBet[user_id] then --已经下注
		print("_________已下注过_____")
		return
	end	
	self.tbPlayerBet[user_id] = rate

	local data = {nnUserBetNt = {
		desk_id = self.desk_id, 
		user_id = user_id,
		seat_id = user:getSeatId(),
		rate = rate,
	}}
    self:sendToAllUser(data)

    local betNum = 0 --已下注人数
    local playingNum = 0
    for k,v in pairs(self.player_list) do    	
		playingNum = playingNum + 1
		if self.tbPlayerBet[k] then
			betNum = betNum + 1				
		end	
    end
    if playingNum > 0 and betNum == playingNum - 1 then --全部下注了提前发剩余牌
    	local callback = function ()
			self.fsm:fire("to_leftcard")
		end
		self:setTimer(1, callback)
    end

	return true
end

--请求亮牌
function Desk:userOpenCard(user_id)
	if self:getState() ~= "leftcard"  then
		return
	end
	if self.player_list[user_id].openCard then 
		--已亮牌
	end
	self.player_list[user_id].openCard = true
	self.player_list[user_id].doNothingNum = 0
	local playerNiuRes = self.rule:getNiu(self.tbPlayerCard[user_id])
	local playerCardType = self.rule:getCardType(playerNiuRes.weight)	
	local data = {nnUserOpenCardNt={
		desk_id = self.desk_id, 
		user_id = user_id, 
		seatid =self.player_list[user_id].seatid,
		cards = playerNiuRes.card,
		card_type = playerCardType,
	}}
    self:sendToAllUser(data)

    local openCardPlayingNum = 0 
    local playingNum = 0
    for k,v in pairs(self.player_list) do
    	if v.playing then
    		playingNum = playingNum + 1
			if v.openCard then
				openCardPlayingNum = openCardPlayingNum + 1				
			end
		end
    end
    if playingNum > 0 and openCardPlayingNum == playingNum then
    	--所有人都已亮牌
    	local callback = function ()
			self.fsm:fire("to_settlement")
		end
		self:setTimer(1, callback)
    end
	return {}
end


-------------------------------------------------------------------------------
-- 推送消息
-------------------------------------------------------------------------------
function Desk:ntWait()
	local data = {nnWaitNt={
 		desk_id = self.desk_id,	
	}}
	self:sendToAllUser(data)
end

function Desk:ntRest()
	self:reset()
	local data = {nnRestNt={
 		desk_id = self.desk_id,	
 		time = self.stateTime.rest
	}}
	print("_________ps_ntRest", data)
	self:sendToAllUser(data)	
end

function Desk:ntStart()
	local user_list = self.user_manager:getUserList()
	for user_id, v in pairs(user_list) do
		local data = { nnStartNt={
	 		desk_id = self.desk_id,	
	 		time = self.stateTime.start, 
	 		cards = self.tbPlayerCard[user_id],		
	 		-- playingid_list = self:getPlayingid(), 		
		}}		
		self:sendToUser(user_id, data)
	end	
	log.debug("__________ps_ntStart", data)
end

function Desk:ntBank()	
	for k, v in pairs(self.player_list) do
		local rate = self:getRaceBankRate(v.gold)		
		local data = { nnBankNt = {
			time = self.stateTime.bank,
			max_bank_rate = rate,
		}}
		self:sendToUser(k, data)
	end	
end

function Desk:ntBet()	
	for k, user in pairs(self.player_list) do	
		local gold = user:getGold()
		local rate = self:getPlayerRate(gold)	
		local data = {nnBetNt = {
			time = self.stateTime.bet,
			max_bet_rate = rate,
		}}
		self:sendToUser(k, data)
	end	
end

--发第5张牌
function Desk:ntLeftCard()	
	for k, v in pairs(self.player_list) do	
		local cards = {self.tbPlayerCard[k][#self.tbPlayerCard[k]]}
		local data = {nnLeftCardNt = {
			time = self.stateTime.leftcard,
			cards = cards,
		}}
		self:sendToUser(k, data)
	end	
end



-------------------------------------------------------------------------------
-- 状态
-------------------------------------------------------------------------------
function Desk:onWait()
	log.debug("_______ps onWait")
	self.operStartTime = os.time()
	--通知客户端
	self:ntWait()

	local function callback()
		local user_list = self.user_manager:getUserList()
		--人数不足时不开始 返回等待状态
		print("__111__onWait__",table.size(user_list) , self.min_player_num)
		if table.size(user_list) < self.min_player_num then 
			return
		end
		self:stopTimer()
		self.fsm:fire("to_start")
	end
	--等待一定时间后开始游戏
	self:setTimer(self.stateTime.wait, callback, true)	
	
end

--休息
function Desk:onRest()
	log.debug("_______ps onRest")
	self.operStartTime = os.time()

	--重置数据
	self:reset()
	self:ntRest()
	self:removeDisconnect()	
	-- self:checkUser()

	local callback = function ( ... )	
		local user_list = self.user_manager:getUserList()
		if table.size(user_list) < self.min_player_num then
			self.fsm:fire("to_wait")
			return
		end			
		self.fsm:fire("to_start")		
	end
	self:setTimer(self.stateTime.rest, callback)
	-- self:checkRemoveRobot()
end

--开始
function Desk:onStart()
	log.debug("_______ps onStart")		
	self.trdid = self.trdid + 1	
	--设置在线玩家开始
	self.user_manager:setAllUserValue("playing", true)

	local user_list = self.user_manager:getUserList()
	self.player_list = user_list --正在玩的用户
	self.playing_num = table.size(self.player_list)
	for k, v in pairs(user_list) do 
		table.insert(self.tbuser_id,k)	
	end
	self.seat_user = self.user_manager:getSeatUser()
	--取牌
	for user_id, _ in pairs(user_list) do
		self.tbPlayerCard[user_id] = self.rule:getCard(4)
	end	
	self:ntStart()

	--开始下注
	self.operStartTime = os.time()	
	local callback = function ()
		--停止下注
		self.fsm:fire("to_bank")
	end
	self:setTimer(self.stateTime.start, callback)
	-- skynet.timeout(math.random(50, 200), function ()
	-- 			self:checkAddRobot()
	-- 		end)
end

--抢庄
function Desk:onBank()
	log.debug("_______ps onBank")
	self.operStartTime = os.time()
	self:ntBank()
	local callback = function ()
		self.fsm:fire("to_bank_ok")
	end
	self:setTimer(self.stateTime.bank, callback)
end

--确定庄
function Desk:onBankOK()
	log.debug("_______ps onBankOk", self.tbRaceBankRate)
	
	self.bankData = {}
	local tbRatePlayer = {}	--叫此倍数的玩家
	local maxBankRate = 0 --最大抢庄倍数
	for user_id, rate in pairs(self.tbRaceBankRate) do		
		if maxBankRate < rate then
			maxBankRate = rate
		end
		tbRatePlayer[rate] = tbRatePlayer[rate] or  {}
		table.insert(tbRatePlayer[rate], user_id)
	end

	local allRate = {-1,-1,-1,-1,-1,-1,} --全部位置上的倍数
	for user_id, user in pairs(self.seat_user) do 		
		allRate[user:getSeatId()]=self.tbRaceBankRate[user_id] or 0
	end

	local data = {} --要发送的数据
	if maxBankRate and maxBankRate > 0 then	--有人抢庄
		if #tbRatePlayer[maxBankRate] == 1 then
			--只用一个人叫庄
			local banker_id = tbRatePlayer[maxBankRate][1]
			self.bankData = {rate = maxBankRate, user_id = banker_id}
		elseif #tbRatePlayer[maxBankRate] > 1 then	
			--多人抢庄相同的倍数，随机
			local r = math.random(1, #tbRatePlayer[maxBankRate])		
			local num = table.size(tbRatePlayer[maxBankRate])	
			local banker_id = tbRatePlayer[maxBankRate][r]
			self.bankData = {rate = maxBankRate, user_id = banker_id}
		end
	else --没人抢庄，随机 				
		local r = math.random(1, #self.tbuser_id)		
		local user_id = self.tbuser_id[r]
		self.bankData = {rate = 1, user_id = user_id}
	end
	data = {nnBankOkNt = {
		desk_id = self.desk_id, 
		rate = self.bankData.rate, 
		allrate = allRate,
		user_id = self.bankData.user_id,
		time = self.stateTime.bankok,
		-- arr_user_id = tbuser_id,
	}}
	self.operStartTime = os.time()

	print("___bankok data:", data)
	self:sendToAllUser(data)

	local callback = function ()
		self.fsm:fire("to_bet")
	end	
	self:setTimer(self.stateTime.bankok, callback)

end

function Desk:onBet()
	log.debug("_______ps onBet")
	self.operStartTime = os.time()
	self:ntBet()
	local callback = function ()
		self.fsm:fire("to_leftcard")
	end	
	self:setTimer(self.stateTime.bet, callback)
end

--发剩余牌
function Desk:onLeftCard()
	log.debug("_______ps onLeftCard")	
	
	for user_id, _ in pairs(self.player_list) do
		if user_id ~= self.bankData.user_id then
			--没有下注的默认选第一个值
			self.tbPlayerBet[user_id] = self.tbPlayerBet[user_id] or self:getConfig().bet_rate[1]
		end
		table.insert(self.tbPlayerCard[user_id], self.rule:getCard(1)[1])
	end	
	self:ntLeftCard()
	self.operStartTime = os.time()	
	local callback = function ()
		self.fsm:fire("to_settlement")
	end
	self:setTimer(self.stateTime.leftcard, callback)
end

--结算
function Desk:onSettlement()
	log.debug("_______ps onSettlement")
	self.operStartTime = os.time()

	-- self:beforRecord()	
	local settlementData = {} --结算数据
	local tbuser_id = self.player_list
	local bankCard = self.tbPlayerCard[self.bankData.user_id] --庄的牌
	local bankNiuRes = self.rule:getNiu(bankCard) --庄算牛结果
	local bankCardType = self.rule:getCardType(bankNiuRes.weight) --庄牌型
	local bankWinChip = 0 --庄赢的筹码数
	local banker_id = self.bankData.user_id
	--闲家结算
	for k, v in pairs(self.tbPlayerBet) do
		local player = self.player_list[k]
		if banker_id ~= player.user_id then
		    local bet = self.bottom_chip * self.bankData.rate * v 
		    local playerNiuRes = self.rule:getNiu(self.tbPlayerCard[k])
		    local playerCardType = self.rule:getCardType(playerNiuRes.weight)
		    -- print("______playerCardType___",playerCardType)
		    local playerSettlement = {user_id = player.user_id, cards = playerNiuRes.card, card_type = playerCardType}
		    if bankNiuRes.weight > playerNiuRes.weight then
		    	--庄赢
		    	bankWinChip = bankWinChip + bet * self.rule:getRate(bankNiuRes.weight)
		    	playerSettlement.winChips = -bet * self.rule:getRate(bankNiuRes.weight)
		    else
		    	bankWinChip = bankWinChip - bet * self.rule:getRate(playerNiuRes.weight)
		    	-- local severFee = math.floor(bet * self.rule:getRate(playerNiuRes.weight) * self.severFeeRate)  
		    	-- playerSettlement.winChips = bet * self.rule:getRate(playerNiuRes.weight) - severFee   	
		    	-- self.record.severFee[player.user_id] = severFee
		    end
		    table.insert(settlementData, playerSettlement) 
		end
	end


	--庄家结算
	local bankSettlement = {}
	bankSettlement.cards = bankNiuRes.card
	bankSettlement.card_type = bankCardType
	bankSettlement.user_id = banker_id
	-- if bankWinChip > 0 then
	-- 	local severFee = math.floor(bankWinChip * self.severFeeRate)
	-- 	bankWinChip = bankWinChip - severFee
	-- 	self.record.severFee[banker_id] = severFee
	-- end
	bankSettlement.winChips = bankWinChip
	table.insert(settlementData, bankSettlement)
	--金钱改变，操作数据库
	for k, v in pairs(settlementData) do
		local player = self.player_list[v.user_id]
		-- local reason = {
		-- 		code = self.roomCfg[self.roomid%10].settlementMoneyCode,
		-- 		moneytype = 1,
		-- 		game = player.game,
		-- 		gameid = self.gameid%10,
		-- 		gametype = self.roomid,
		-- 	}		
		--print("==========", player.user_id, v.winChips)
		-- local money = self:callGate(player, 'change_money', v.winChips, reason)
		-- print("-------------------->settlement:", self.desk_id, v.user_id, money)
		-- if money then
		-- 	player.chip = money
		-- else
		-- 	log.debug("------------------>ps settlement", "winChips:", v.winChips, "player.chip:", player.chip)
		-- end
		-- v.chip = player.chip
	end
	self:sendToAllUser({nnEndNt={
		table_id = self.desk_id,
		player_settle = settlementData,
		time = self.stateTime.settlement
	}})	
	-- self.record.settlement = settlementData	

				
	local callback = function ()
		--休息
		self.fsm:fire("to_rest")
	end
	self:setTimer(self.stateTime.settlement, callback)
	-- self:doRecord()
end

-- function Desk:beforRecord()
-- 	self.record = {severFee = {}, settlement = {}, beforeChip = {}}
-- 	self.record.betRate = self.tbPlayerBet
-- 	for k, v in pairs(self.player_list) do
-- 		if v.playing then
-- 			table.insert(self.record.beforeChip, {user_id = v.user_id, chip = v.chip})
-- 		end
-- 	end
-- end

-- function Desk:doRecord()
-- 	self.record.bottomChip = self.bottom_chip
-- 	self.record.roomid = self.roomid
-- 	self.record.bankData = self.bankData
-- 	local time = os.time()

-- 	-- 服务费
-- 	for user_id, severFee in pairs(self.record.severFee) do
-- 		local player = self.player_list[user_id]
-- 		local bank = 2
-- 		if self.bankData.user_id == user_id then
-- 			bank = 1	
-- 		end
-- 		local tableName = 'service_charge_log'.. os.date("%Y%m%d")
-- 		local str = tableName .." (uid, charge, banker, sid, time, game, gameid, gametype)"
-- 		local sql = string.format("insert into %s values(%d,%d,%d,%d,%d,%d,%d,%d);",
-- 			str, user_id, severFee, bank, player.seatid or 0, time, player.game or 0, self.gameid%10, self.roomid)
-- 		local r = skynet.send("mysql_log", "lua", "execute", sql)
-- 	end

-- 	local tableName = 'qclandlord_tablelog'
	
-- 	local str = tableName .." (tid, trdid, svid, tlcnt, tllog, tltime, game, gametype)"
-- 	local sql = string.format("insert into %s values(%d,%d,%d,%d,'%s',%d,%d,%d);",
-- 		str, self.desk_id, self.trdid, 10, table.size(self.player_list), cjson.encode(self.record), time, self.gameid%10, self.roomid)
-- 	local res = skynet.call("mysql_log", "lua", "execute", sql)

-- 	local tlid = res.insert_id
-- 	for k, v in pairs(self.record.settlement) do
-- 		local wlfold
-- 		if v.user_id == self.bankData.user_id then
-- 			wlfold = self.bankData.rate
-- 		else
-- 			wlfold = self.tbPlayerBet[v.user_id]
-- 		end		
-- 		local player = self.player_list[v.user_id]
-- 		local wlleft = player.chip
-- 		local wlwin = v.winChips
-- 		local wlflag
-- 		if wlwin > 0 then wlflag = 1 else wlflag = 0 end		

-- 		local str1 = 'qclandlord_winlog' .. " (tlid, uid, pfid, usid, tid, room_id, svid, trdid, wlfold, wlleft, wlwin, wlflag, wltime, game, gameid, gametype)"
-- 		local sql1 = string.format("insert into %s values(%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d);", str1, tlid, v.user_id, player.pfid or 0, player.usid or 0,self.desk_id, self.roomid, 9,
-- 														self.trdid, wlfold, wlleft, wlwin, wlflag, time, player.game or 0,self.gameid%10, self.roomid)
-- 		local res = skynet.call("mysql_log", "lua", "execute", sql1)
-- 		--print("-------------------------------->res sql1",res, sql1)
-- 	end


-- end

function Desk:getPlayerCard(user_id)
	return self.tbPlayerCard[user_id] or {}
end

function Desk:getPlayerCardType(user_id)

end

--取桌子状态
function Desk:getDeskStatus(user_id)
	--各个状态下玩家信息会有所有同
	local config = self:getConfig()
	local extend = {
		config = config,
	}
	return cjson.encode(extend)
end



--
function Desk:sitDownCallback(user_id)
	print("------------------------------------>sitDownCallback", user_id)
	local player = self:getPlayerInfo(user_id)
	local data = {ps_nt_seat_change = {
		desk_id = self.desk_id,
		seat_player = player,
		seat = true,
	}}	
	self:sendToOther(user_id, data)
end


--
function Desk:sitUpCallback(seatid, user_id)
	local player = self:getPlayerInfo(user_id)
	local data = {ps_nt_seat_change = {
		desk_id = self.desk_id,
		seat_player = player,
		seat = false,
	}}	
	self:sendToOther(user_id, data)
end



-----------------------------------------------------------------------------------
----------------------------------
-- 数据操作
----------------------------------



--桌子相关配置
function Desk:getConfig()
	local bank_rate = {1,2,3}
	local bet_rate = {1,2,3}
	return {time_rest = self.stateTime.rest,
			time_start = self.stateTime.start,
			time_card = self.stateTime.card,
			time_end = self.stateTime.settlement,
			bank_rate = bank_rate,
			bet_rate = bet_rate,
			room_type = 1,
			bottom_chip = self.bottom_chip, --底分
	}
end


--抢庄赔率计算
function Desk:getRaceBankRate(chip)
	local rate = chip/(self.bottom_chip * (self.playing_num - 1) * self:getMaxPlayerRate() * self:getMaxCardRate())
	return math.floor(rate) 
end

--计算闲家可下注赔率
function Desk:getPlayerRate(chip)
	local bankPlayer = self.player_list[self.bankData.user_id]
	local rate =  chip /(self.bottom_chip *(self.playing_num - 1) * self:getMaxCardRate() * self.bankData.rate)	
	return math.floor(rate) 
end

--闲家下注最高倍数
function Desk:getMaxPlayerRate()
	local bet_rate  = self:getConfig().bet_rate
	return bet_rate[#bet_rate]
end

--牌型最高赔率
function Desk:getMaxCardRate()
	return self.rule:getRate(self.rule.tbWeight.wxn)
end




return Desk