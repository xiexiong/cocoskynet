--
-- Author:      name
-- DateTime:    2018-04-23 17:19:33
-- Description: 管理大厅socket, service
require "skynet.manager"
local skynet = require "skynet"
local NodeMessage = require "NodeMessage"
local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "manager.MessageHandler"
local ClusterClient = require "ClusterClient"
local ServerManager = require "ServerManager"
local RoomManager = require "RoomManager"


--服务唯一全局变量
global = {}

local function init()

	local node_message = NodeMessage.new()
	local message_dispatch = MessageDispatch.new()	
	local message_handler = MessageHandler.new(message_dispatch, node_message)	
	local cluster_client = ClusterClient.new(message_dispatch, node_message)
	local server_manager = ServerManager.new()
	local room_manager = RoomManager.new()


	global.node_message = node_message
	global.message_dispatch = message_dispatch
	global.message_handler = message_handler
	global.server_manager = server_manager
	global.room_manager = room_manager


				
	--开启集群节点
	cluster_client:openCluster()
	--lua消息的派发
	skynet.dispatch("lua", message_dispatch:dispatch())
end

---------------------------------------------------------
-- skynet
---------------------------------------------------------

skynet.start(function()

	init()

	skynet.register('.proxy')

end)


--