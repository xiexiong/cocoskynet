--
-- Author:      name
-- DateTime:    2018-04-23 17:20:15
-- Description: 处理socket消息的服务

local skynet = require "skynet"
local log = require "Logger"

local MessageDispatch = require "MessageDispatch"
local NodeMessage = require "NodeMessage"
local MessageHandler = require "gate.MessageHandler"
local SessionManager = require "gate.SessionManager"
local UserManager = require "gate.UserManager"
local LoginHandler = require "gate.LoginHandler"

global = {}

--此服务用到的对象
local function init()

	--会话管理
	local session_manager = SessionManager.new()
	--用户数据
	local user_manager = UserManager.new()	
	--节点消息
	local node_message = NodeMessage.new()
	--消息派发
	local message_dispatch = MessageDispatch.new()
	--处理服务间消息
	local message_handler = MessageHandler.new(message_dispatch, node_message, session_manager, user_manager)
	--登陆处理
	local login_handler = LoginHandler.new(message_dispatch, node_message, session_manager, user_manager)

	global.session_manager = session_manager
	global.user_manager = user_manager
	global.node_message = node_message
	global.message_dispatch = message_dispatch
	global.message_handler = message_handler
	global.login_handler = login_handler


	--socket消息
	skynet.register_protocol {
		name = "client",
		id = skynet.PTYPE_CLIENT,
		--消息解包
		unpack = handler(message_handler, message_handler.messageUnpack),
		--从客户发来的消息
		dispatch = handler(message_handler, message_handler.dispatchClientMessage),		
	}
	--处理服务消息
	skynet.dispatch("lua", message_dispatch:dispatch())

end


-----------------------------------------------------------------------------------------
--skyent
-----------------------------------------------------------------------------------------

skynet.start(function()

	init()

end)