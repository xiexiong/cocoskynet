--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 节点内消息的处理
require "skynet.manager"
local skynet = require "skynet"
local log = require "Logger"
-- local crypt = require "crypt"
local cluster = require "cluster"
local config = require "configquery"

local ClusterManager = class("ClusterManager")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function ClusterManager:ctor(message_dispatch, node_message)

	self.message_dispatch = message_dispatch	
	self.node_message = node_message
	self.cluster_list = {} --集群列表
	self.cluster_time = {} --服务器最后的心跳时间
	self.server_list = {} --服务器列表


	self:register()
end

--注册本服务里的消息
function ClusterManager:register()
	-- self.message_dispatch:registerSelf('manager_start',handler(self, self.managerStart))

	self.message_dispatch:registerSelf('cluster_heartbeat_req',handler(self, self.onHeartBeatReq))
	self.message_dispatch:registerSelf('cluster_connect_req',handler(self, self.onConnectReq))
	
	self.message_dispatch:registerSelf('cluster_hotfix_req',handler(self,self.onClusterHotfixReq))
	self.message_dispatch:registerSelf('get_server_list',handler(self,self.getServerList))
end

-- 从db加载服务器配置
function ClusterManager:loadClusterFromDB()
 	local server_list = self.node_message:callDbProxy("load_server_list")
 	-- print("___________server___list__",server_list)
 	self.server_list = server_list
 	-- 把集群列表发给已上线的服务器
 	local cluster_list = {}
 	for k,v in pairs(server_list) do 
 		cluster_list["s"..v.server_id] = v.ip ..":".. v.port
 	end
 	cluster.reload(cluster_list)
 	skynet.fork(function()
 		skynet.sleep(6*100)
	 	for k,v in pairs(cluster_list) do 
	 		print("_________reload_cluster_nt__",k,v)
	 		self.node_message:sendNodeProxy(k, 'reload_cluster_nt', cluster_list)
	 	end	 
	 end)
 	self.cluster_list = cluster_list
end

-- 检查是否断线
function ClusterManager:update()
	for k, v in pairs(self.cluster_time) do 
		if os.time() - v > 60 then 
			self.node_message:sendDbProxy("update_server_list",{ online=0}, {{server_id=self.cluster_list[k].server_id}})
		end
	end
end
---------------------------------------------------------
-- CMD
---------------------------------------------------------

-- 负责监控集群的服务
function ClusterManager:managerStart()
	--是管理者	
	--数据库,加入需要的表
	local tables = {
		{name="server_list", pk='server_id', key='server_id', no_use_redis=true },
	}
 	self.node_message:callDbProxy("load_tables",tables)
 	self:loadClusterFromDB()

	skynet.fork(function()
		while true do 
			skynet.sleep(100)
			self:update()
		end
	end)	 		

end

--收到心跳包
function ClusterManager:onHeartBeatReq(data)
	-- print("______onHeartBeatReq___",data)
	for k, v in pairs(self.cluster_list) do 
		if data.server_id == v.server_id then 		
			self.cluster_time[k] = os.time()
			break
		end
	end	
	self.node_message:sendDbProxy("update_server_list", { online=1}, {{server_id=data.server_id}})
	return true
end

-- 后台通知更新集群列表
function ClusterManager:onClusterHotfixReq()
	self:loadClusterFromDB()
	return true
end

-- 散服务器请求集群列表
function ClusterManager:onConnectReq(cluster_name)
 	-- for k,v in pairs(self.cluster_list) do 
 	-- 	self.node_message:sendNode(k, 'cluster_manager', 'reload_cluster_nt', self.cluster_list)
 	-- end	 
 	print("_____cluster_name__",cluster_name)
 	if not next(self.cluster_list) then 
 		return 
 	end
 	self.node_message:sendNodeProxy(cluster_name, 'reload_cluster_nt', self.cluster_list)
	return true
end

-- 请求返回服务器列表
function ClusterManager:getServerList()
	local server_list = {}
	for k,v in pairs(self.server_list) do 
		server_list[v.server_id] = v
	end
	return server_list
end

return ClusterManager