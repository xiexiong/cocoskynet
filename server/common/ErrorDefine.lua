
--
-- @Author:      dreamfly
-- @DateTime:    2019-05-06 11:02:03
-- @Description: 游戏错误定义

--游戏错误
GameError =
{
	--基本错误
    Success								= 0,				--没有错误
    Error                               = 1,                --发生错误
	Failure								= 1,				--失败错误
	
	--系统错误
	Socket								= 10,				--网络错误
	Timer								= 11,				--定时错误
	Database							= 12,				--数据库错误
	Command								= 13,				--命令错误
	DataError							= 14,				--数据错误
	Parameter							= 15,				--参数错误
	Exception							= 16,				--处理异常
    Maintain							= 17,				--系统维护
    
	--注册错误
	Register							= 100,				--注册错误
	ForbidRegister						= 101,				--禁止注册
	ForbidRegisterIP					= 102,				--禁止登录IP
	ForbidRegisterMachine				= 103,				--禁止登录机器
	AccountExist						= 104,				--账号已存在

	--登录错误
	Login								= 110,
	ForbidLogin							= 111,				--禁止登录(db)
	ForbidLoginIP						= 112,				--禁止登录IP
	ForbidLoginMachine					= 113,				--禁止登录机器
	AccountNotExist						= 114,				--账号不存在
	AccountFrozen						= 115,				--账号冻结
	AccountShutDown						= 116,				--账号关闭
	BindMachine							= 117,				--绑定机器
	PasswordError						= 118,				--密码错误
	GoldException						= 119,				--金币异常
	LockGoldException					= 120,				--锁金币异常
	BankOperate							= 121,				--银行操作
	LockServer							= 120,				--锁在服务器

	--服务器错误
	InvaidServer						= 200,				--无效服务器
	HallVersion						    = 201,				--大厅版本
	FrameVersion						= 202,				--框架版本
	ClientVersion						= 203,				--客户端版本
	InServerGame						= 204,				--在房间游戏

	GameLogin							= 220,				--游戏登录
	KickUser							= 221,				--踢出用户
	FullConnect							= 225,				--连接已满
	FullServer							= 226,				--房间已满
	CannotAssignUser					= 227,				--不能分配用户

	SwitchConnect						= 260,				--切换连接
	NonlocalLogin						= 261,				--异地登录
	
	--权限错误
	UserRight							= 300,				--权限错误					
	UserForbid							= 301,				--用户禁止
	UserLeave							= 302,				--用户离开	

	--进入错误
	ForbidEnter							= 400,				--禁止进入
	EnterGame							= 401,				--进入游戏
	EnterMatch							= 402,				--进入比赛场
	EnterPrivate						= 403,				--进入私人场

	--离开错误
    ForbidLeave							= 500,				--离开错误
    
    --坐下错误	
	ForbidSitDown						= 600,				--禁止坐下
	NoMatchDesk						    = 601,				--无匹配桌子
	NoMatchSeat						    = 602,				--无匹配座位
	FullDesk							= 603,				--人数已满	
	FullSeat							= 604,				--座位已满
	SeatOccupy							= 605,				--座位占用
	DeskPasswordError					= 606,				--桌子密码错误

	--旁观错误
	ForbidLookon						= 700,				--禁止旁观
	ForbidUserLookon					= 701,				--禁止用户旁观
	NoLookonRight						= 702,				--没有旁观权限	


	--直播错误
	ForbidLive							= 800,				--禁止直播
	ForbidUserLive						= 801,				--禁止用户直播
	NoLiveRight							= 802,				--没有直播权限


	--站起错误
	ForbidStandUp						= 900,				--禁止站起	

    --游戏错误
    WaitGameStart						= 1000,				--等待游戏开始
	WaitGameEnd							= 1001,				--等待游戏结束
	GameStart							= 1002,				--游戏开始
	GameEnd								= 1003,				--游戏结束
	GameDismiss							= 1004,				--游戏解散
	GamePlaying							= 1005,				--正在游戏中
	GameMatch							= 1006,				--正在比赛中
	GamePrivate							= 1007,				--正在私场中

	--继续游戏错误
    ForbidContinueGame					= 1400,				--禁止继续游戏
    
    --切换用户状态错误
	SwitchForbidSitDown				    = 1500,				--禁止座下
	SwitchForbidLookon					= 1501,				--禁止旁观
	SwitchForbidSwitch					= 1502,				--禁止切换
	SwitchFullDesk					    = 1503,				--人数已满
	SwitchGameStart					    = 1504,				--游戏开始
	SwitchSeatOccupy					= 1505,				--椅子占用
	SwitchWaitGameStart				    = 1506,				--等待游戏开始
	SwitchUserLeave					    = 1507,				--用户离开

	--分数限制错误
	LimitEnterGameScore					= 1600,				--限制进入游戏分数
	LimitMinEnterGameScore				= 1601,				--限制最小进入游戏分数
	LimitMaxEnterGameScore				= 1602,				--限制最大进入游戏分数
	LimitMinEnterGold					= 1603,				--限制最小进入金币
	LimitMaxEnterGold					= 1604,				--限制最大进入金币
	LimitMinEnterScore					= 1605,				--限制最小进入积分
	LimitMaxEnterScore					= 1606,				--限制最大进入积分
	LimitMinEnterGrade					= 1607,				--限制最小进入成绩
	LimitMaxEnterGrade					= 1608,				--限制最大进入成绩
	LimitMinEnterInsure					= 1609,				--限制最小进入银行
	LimitMaxEnterInsure					= 1610,				--限制最大进入银行
	LimitMinEnterDiamond				= 1611,				--限制最小进入钻石
	LimitMaxEnterDiamond				= 1612,				--限制最大进入钻石
	LimitMinEnterIngot					= 1613,				--限制最小进入元宝
	LimitMaxEnterIngot					= 1614,				--限制最大进入元宝
	LimitMinEnterTicket					= 1615,				--限制最小进入券分
	LimitMaxEnterTicket					= 1616,				--限制最大进入券分

    --会员限制错误
	LimitEnterMember					= 1630,				--限制进入会员等级
	LimitMinEnterMember					= 1631,				--限制最小进入会员
	LimitMaxEnterMember					= 1632,				--限制最大进入会员
    
    --条件限制错误
	LimitSameIP							= 1650,				--限制相同IP
	LimitWinRate						= 1651,				--限制胜率
	LimitFleeRate						= 1652,				--限制逃跑率
	LimitGameScore						= 1653,				--限制游戏分
	LimitMinGameScore					= 1654,				--限制最低游戏分
	LimitMaxGameScore					= 1655,				--限制最高游戏分

	--缺少游戏分错误
	LackGold							= 1700,				--金币不足
	LackScore							= 1701,				--积分不足
	LackGrade							= 1702,				--成绩太低
	LackInsure							= 1703,				--银行不足
	LackDiamond							= 1704,				--钻石不足
	LackIngot							= 1705,				--元宝不足
	LackTicket							= 1706,				--券分不足

    --比赛场
    ForbidMatch                         = 1800,

	--私人场
	ForbidCreatePrivate					= 1900,				--禁止创建
	ForbidProxyPrivate					= 1901,				--禁止代理
	ForbidJoinPrivate					= 1902,				--禁止加入
	ForbidDismissPrivate				= 1903,				--禁止解散
	PrivateRoomFull						= 1904,				--房间已满
	PrivateProxyLimitCount				= 1905,				--代开房限制
	PrivatePlayerCount					= 1906,				--游戏人数
	PrivatePlayCount					= 1907,				--游戏局数
	PrivatePayCostMode					= 1908,				--支付模式
	PrivateCostType						= 1910,				--费用类型
	PrivateRoomType						= 1911,				--房间类型
	PrivateRoomFlag						= 1912,				--房间标志
	PrivateGameType						= 1913,				--游戏类型
	PrivateGameRule						= 1914,				--游戏规则
	PrivateFixedGrade					= 1915,				--固定分数
	PrivateRestrictScore				= 1916,				--限制分数
													  
	--擂台场错误
	ForbidCreateChallenge				= 2000,				--禁止创建
	ForbidJoinChallenge					= 2001,				--禁止加入
	ForbidDismissChallenge				= 2002,				--禁止解散
	ChallengeRoomFull					= 2003,				--房间已满
	ChallengePlayerCount				= 2004,				--游戏人数
	ChallengePlayCount					= 2005,				--游戏局数
	ChallengePayCostMode				= 2006,				--支付模式
	ChallengeCostType					= 2007,				--费用类型
	ChallengeRoomType					= 2008,				--房间类型
	ChallengeRoomFlag					= 2009,				--房间标志
	ChallengeGameType					= 2010,				--游戏类型
	ChallengeGameRule					= 2011,				--游戏规则
	ChallengeFixedGrade					= 2012,				--固定分数
	ChallengeRestrictScore				= 2013,				--限制分数

	--俱乐部错误
	ForbidCreateClub					= 2100,				--禁止创建
	ForbidProxyClub						= 2101,				--禁止代理
	ForbidJoinClub						= 2102,				--禁止加入
	ForbidDismissClub					= 2103,				--禁止解散
	ClubRoomFull						= 2104,				--房间已满
	ClubPlayerCount						= 2105,				--游戏人数
	ClubPlayCount						= 2106,				--游戏局数
	ClubPayCostMode						= 2107,				--支付模式
	ClubCostType						= 2108,				--费用类型
	ClubRoomType						= 2109,				--房间类型
	ClubRoomFlag						= 2110,				--房间标志
	ClubGameType						= 2111,				--游戏类型
	ClubGameRule						= 2112,				--游戏规则
	ClubFixedGrade						= 2113,				--固定分数
	ClubRestrictScore					= 2114,				--限制分数
}




