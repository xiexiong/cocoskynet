--
-- Author:      name
-- DateTime:    2018-04-23 17:19:33
-- Description: 服务管理


require "skynet.manager"
local skynet = require "skynet"
local NodeMessage = require "NodeMessage"
local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "MessageHandler"
local ClusterClient = require "ClusterClient"

global = {}

local function init()

	local message_dispatch = MessageDispatch.new()	
	local node_message = NodeMessage.new()
	local message_handler = MessageHandler.new(message_dispatch, node_message)
	local cluster_client = ClusterClient.new(message_dispatch, node_message)
	-- self.cluster_manager = ClusterManager.new(self.message_dispatch, self.node_message)
	-- self.server_handler = ServerHandler.new(self.message_dispatch, self.node_message)

	--开启集群节点
	cluster_client:openCluster()
	skynet.dispatch("lua", message_dispatch:dispatch())

end


---------------------------------------------------------
-- skynet
---------------------------------------------------------

skynet.start(function()

	init()

	skynet.register('.proxy')

end)