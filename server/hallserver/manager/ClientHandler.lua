--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 客户端消息的处理

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local crypt = require "skynet.crypt"


local ClientHandler = class("ClientHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function ClientHandler:ctor(message_dispatch, node_message)

    self.server_id = tonumber(skynet.getenv("server_id")) or 0
    self.server_type = skynet.getenv("server_type") or ""
    self.server_name = skynet.getenv("server_name") or ""
    self.cluster_addr = skynet.getenv("cluster_addr")
    self.cluster_name = skynet.getenv("cluster_name")
    self.server_name = skynet.getenv("server_name")
    self.zone_cluster_id = skynet.getenv("zone_cluster_id")
    self.node_cluster_id = skynet.getenv("node_cluster_id")

	self.message_dispatch = message_dispatch	
	self.node_message = node_message
    self.server_manager = nil 

	self:register()
end

--注册本服务里的消息
function ClientHandler:register()

	self:registerSelf('enterHallReq')
	self:registerSelf('enterGameReq')	
end

function ClientHandler:registerSelf(message_name)
	if not self[message_name] then 
		log.error("__registerSelf_没有注册函数回调____",message_name)
	end
	self.message_dispatch:registerSelf(message_name, handler(self,self[message_name]))
end

function ClientHandler:init()
    self.server_manager = global.server_manager
end

---------------------------------------------------------
-- CMD
---------------------------------------------------------

--用户注册
function ClientHandler:enterHallReq(data)
	print("___enterHallReq__",data)
	local server_manager = self.server_manager
    --在线信息
    server_manager:increaseOnlineCount()
    local online_info = {
        server_id = self.server_id,
        server_type = self.server_type,
        max_client = server_manager:getMaxClient(),
        online_count = server_manager:getOnlineCount(),
    }
    --发送在线信息
    self.node_message:sendZoneProxy("server_online_info_nt", online_info)


    --返回给客户端
	local res = {
		enterHallRes={
			error_code=0,
			error_msg="进入成功"
		}
	}
	return res
end

--进入游戏
function ClientHandler:enterGameReq(msg_data)
    print("___onEnterGame_msg_data__",msg_data)
    local user_id = msg_data.user_id

    local info = {
        user_id = user_id,
        game_id	= msg_data.game_id,
        group_id = msg_data.group_id,
        desk_id = msg_data.desk_id,
    }
    --请求分配桌子
    local assign_result = self.node_message:callNodeProxy(self.zone_cluster_id, 'assign_game_server_req', info)
    if not assign_result or assign_result.error_code ~= 0 then
        log.debug("________分配桌子失败___", assign_result)
        local enter_result = {
            error_code = assign_result and assign_result.error_code or 0,
            error_msg = assign_result and assign_result.error_msg or "分配服务器失败",
        }	
        --发送登陆结果        
        return {enterGameRes = enter_result}
    end
    -- log.debug("________sever_id and desk_id_____", assign_result.server_id, assign_result.desk_id)
    --进入结果
    local enter_result = {
        error_code = 0,
        desk_id = assign_result.desk_id,
    }
    return {enterGameRes = enter_result}
end









return ClientHandler