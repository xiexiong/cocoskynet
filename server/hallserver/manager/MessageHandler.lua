--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 消息的处理

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local ProtoLoader = require "ProtoLoader"

local MessageHandler = class("MessageHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function MessageHandler:ctor(message_dispatch, node_message)
	
	self.proto_file = skynet.getenv("proto_config") --proto文件路径	
    self.server_id = tonumber(skynet.getenv("server_id")) or 0
    self.server_type = skynet.getenv("server_type") or ""
    self.server_name = skynet.getenv("server_name") or ""
    self.cluster_addr = skynet.getenv("cluster_addr")
    self.cluster_name = skynet.getenv("cluster_name")
    self.server_name = skynet.getenv("server_name")
    self.zone_cluster_id = skynet.getenv("zone_cluster_id")
    self.node_cluster_id = skynet.getenv("node_cluster_id")

	self.message_dispatch = message_dispatch
	self.node_message = node_message
	self.proto_loader = ProtoLoader.new() --proto加载

	self:register()
end

--注册本服务里的消息
function MessageHandler:register()
	--启动
	self.message_dispatch:registerSelf('start', handler(self,self.start))
	self.message_dispatch:registerSelf('server_register_res', handler(self,self.serverRegisterRes))
    self.message_dispatch:registerSelf('dispatch', handler(self,self.dispatch))
end

-- 向节点服务器注册
function MessageHandler:serverRegisterReq()
    local server_info = {
        server_id = self.server_id,
        server_type = self.server_type,
        server_name = self.server_name,
        cluster_addr = self.cluster_addr,
        cluster_name = self.cluster_name,
    }
    print("_____发送注册____")
    self.node_message:sendNodeProxy(self.node_cluster_id, "server_register_req", server_info)     
end

------------------------------------------------------------------
--cmd
------------------------------------------------------------------
function MessageHandler:start()

	local pbc_env = self.proto_loader:init(self.proto_file, {'common','game','gate','web','zone','hall',})
    self.server_manager = global.server_manager
	--初始化
	self.node_message:initProto(pbc_env)

    self.client_handler = global.client_handler    
    self.client_handler:init()
    
	--skynet控制台
	-- skynet.newservice("debug_console", svr_config.debug_port)

	self:serverRegisterReq()
end

--nodeserver 返回注册成功
function MessageHandler:serverRegisterRes(msg_data)
    -- print("__serverRegisterRes___",msg_data)
    local server_info = {
        server_id = self.server_id,
        server_type = self.server_type,
        server_name = self.server_name,
        cluster_addr = self.cluster_addr,
        cluster_name = self.cluster_name,        
        max_client = self.server_manager:getMaxClient(),
        online_count = self.server_manager:getOnlineCount(),
    }   
    self.node_message:sendNodeProxy(self.zone_cluster_id, "server_info_nt", server_info)
end

--从nodeserver中转到这里的消息
function MessageHandler:dispatch(cmd, ...)
    
    return self.message_dispatch:dispatchSelfMessage(cmd, ...)

end


return MessageHandler