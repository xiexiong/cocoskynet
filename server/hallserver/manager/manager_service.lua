--
-- Author:      name
-- DateTime:    2018-04-23 17:19:33
-- Description: 管理大厅socket, service
require "skynet.manager"
local skynet = require "skynet"
local NodeMessage = require "NodeMessage"
local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "manager.MessageHandler"
local ClusterClient = require "ClusterClient"
local ClientHandler = require "ClientHandler"
local ServerManager = require "ServerManager"

--服务唯一全局变量
global = {}

local function init()

	local node_message = NodeMessage.new()
	local message_dispatch = MessageDispatch.new()	
	local message_handler = MessageHandler.new(message_dispatch, node_message)
	local client_handler = ClientHandler.new(message_dispatch, node_message)	
	local cluster_client = ClusterClient.new(message_dispatch, node_message)
	local server_manager = ServerManager.new()

	global.node_message = node_message
	global.message_dispatch = message_dispatch
	global.message_handler = message_handler
	global.cluster_client = cluster_client
	global.server_manager = server_manager
	global.client_handler = client_handler
				
	--开启集群节点
	cluster_client:openCluster()
	--lua消息的派发
	skynet.dispatch("lua", message_dispatch:dispatch())
end

---------------------------------------------------------
-- skynet
---------------------------------------------------------

skynet.start(function()

	init()

	skynet.register('.proxy')

end)


--