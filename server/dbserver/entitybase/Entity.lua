

local skynet = require "skynet"
-- Entity
local Entity = class("Entity")

function Entity:ctor()	
	self.tbname = ""			-- 表名	
	self.pk = ""				-- 主键字段
	self.key = ""				-- 查询所使用的字段名
	self.indexkey = ""			-- indexkey 索引key
	self.command = nil			 --操作db的对象,会从子类中传入
	self.no_use_redis = false 	 --使用redis
end


--设置表的主键， 查询字段名，自增字段名
function Entity:init(tb)
	self.pk = tb.pk
	self.key = tb.key
	self.indexkey = tb.indexkey
	self.no_use_redis = tb.no_use_redis

end

-- 返回表操作配置
function Entity:getConfig()
	return {
		name=self.tbname,
		pk=self.pk,
		key=self.key,
		indexkey=self.indexkey,
		no_use_redis = self.no_use_redis,
	}
end

-- 加载表的所有数据
function Entity:load()
	local record = self.command:get(self)		
	return record		
end

-- 再从redis加载到内存
function Entity:reload()
	return self:get()
end

-- 卸载redis数据
function Entity:unload(where_field_values)
	self.command:unload(self, where_field_values)-- redis是移除
end


-- where_field_values 一般为空
-- nosync 异步写入
function Entity:add(field_values, where_field_values, nosync)
	local res = self.command:add(self, field_values, where_field_values, nosync)
	return res
end

-- where_field_values 删除的条件符合的字段
function Entity:delete(where_field_values, nosync)
	local res = self.command:delete(self, where_field_values, nosync)
	return res
end

-- field_values 要更新的字段
function Entity:update(field_values, where_field_values, nosync)	   	
	local res = self.command:update(self, field_values, where_field_values, nosync)
	return res
end

-- 增量修改
function Entity:modify(field_values, where_field_values, nosync)	  	
	local res = self.command:modify(self, field_values, where_field_values, nosync)
	return res
end

-- field_values为空，获取整行记录，返回k,v形式table
-- field_values为一个数组形式table，表示获取数组中指定的字段的值，返回k,v形式table
-- where_field_values 为条件，如{{user_id=1},{is_robot=1}}
function Entity:get(field_values, where_field_values)
	local record = self.command:get(self, field_values, where_field_values)
	if next(record) then return record end
	return nil
end

function Entity:exists( where_field_values )
	local record = self.command:exists(self, field_values, where_field_values)
	return record
end


-- -- 获取redis表的主键id下一个编号
-- function Entity:getNextId()
-- 	return self.command:executeRedis("incr", {  self.tbname .. ":" .. self.pk })
-- end

return Entity
