--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 消息的处理

local skynet = require "skynet"
local sharedata = require "skynet.sharedata"
local log = require "Logger"
local config = require "configquery"
local NodeMessage = require "NodeMessage"
local ProtoLoader = require "ProtoLoader"
local Network = require "Network"


local MessageHandler = class("MessageHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function MessageHandler:ctor(message_dispatch)

	self.server_id = skynet.getenv("server_id") --节点名
	self.robot_name = skynet.getenv("robot_name")
	self.cluster_name = skynet.getenv("cluster_name")
	self.cluster_addr = skynet.getenv("cluster_addr")
	self.proto_file = skynet.getenv("proto_config") --proto文件路径	
	self.message_dispatch = message_dispatch	
	
	-- local ip = "127.0.0.1"
	-- local port = 18002
	-- self.network = Network.new(ip,port) 
	self.node_message = NodeMessage.new()		
	self.proto_loader = ProtoLoader.new() --proto加载

	self:register()

end

--注册本服务里的消息
function MessageHandler:register()

	self.message_dispatch:registerSelf('start',handler(self,self.start))
	self.message_dispatch:registerSelf( "add_robot_to_game", handler(self,self.addRobotNt))

end

-- function MessageHandler:registerByName(package_name, message_name, callback)
-- 	if not callback then 
-- 		callback = handler(self,self[message_name])
-- 	end
-- 	self.network:register(package_name..message_name, callback)
-- end

-- function MessageHandler:registerRobotMessage(message_name, callback)
-- 	local package_name = "proto.msg.robot."
-- 	self:registerByName(package_name,  message_name, callback)
-- end
---------------------------------------------------------
-- CMD
---------------------------------------------------------
function MessageHandler:start()
	print("___proto_file_",self.proto_file)
	local robot_name = skynet.getenv("robot_name")
    local pbc_env = self.proto_loader:init(self.proto_file, {'common','game', 'gate','web','zone','hall',}, {{dir = 'games', files = "msg_"..robot_name..".proto"}})

	self.cmd = config.message_cmd
	self.pbc_env = pbc_env
	--配置文件路径
	local config_path = skynet.getenv("config_root")	
	local config_file = config_path.."robot_zjh_config"
	local data = require(config_file)
	sharedata.new("robot_zjh_config", data)
		-- print("___config__",config.robot_zjh_config)
	-- self.network:init(pbc_env)
	-- self.network:open()

	self:loginReq()
	
end

function MessageHandler:loginReq()
	local data = {

		name = self.robot_name,
		cluster_name = self.cluster_name,
		cluster_addr = self.cluster_addr,
	}
	-- self.network:send(self.cmd.DMSG_MDM_ROBOT,self.cmd.DMSG_RobotLoginReq,data)	
	self.node_message:sendRobotProxy("login_req", data)
end

function MessageHandler:addRobotNt(data)
	print("______addRobotNt___",data)
	--让指定类型的机器人登录
	local robot_name = data.robot_type
	local service = skynet.newservice(robot_name.."_service")		
	skynet.call(service, 'lua', 'init', self.pbc_env, data)
	
end

return MessageHandler