--
-- Author:      name
-- DateTime:    2018-04-23 17:19:33
-- Description: 服务管理


require "skynet.manager"
local skynet = require "skynet"
local cluster = require "skynet.cluster"


local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "MessageHandler"



local function init()

	local message_dispatch = MessageDispatch.new()	
	local message_handler = MessageHandler.new(message_dispatch)

	skynet.dispatch("lua", message_dispatch:dispatch())		
end


---------------------------------------------------------
-- skynet
---------------------------------------------------------

skynet.start(function()

	init()

	skynet.register('.proxy')

	--集群节点
	local server_id = "robot_"..skynet.getenv("robot_name")
	-- print("___server_id__", server_id)
	cluster.open(server_id)	

	-- local server_name = skynet.getenv("svr_name")
	-- print("#####################", server_name)	
end)