# cocoskynet

#### 介绍
框架
![image1](/image1.png)
<bar>
#### 软件架构
软件架构说明
>depends --skynet引擎已编译好 <br>
>server  --游戏框架 <br>
>>common --公用的代码，配置 <br>
>>zoneserver --调度节点 <br>
>>dbserver      --db节点 <br>
>>gameserver    --游戏节点 <br>
>>gateserver    --网光节点 <br>
>>hallserver    --大厅节点 <br>
>>proto         --pb文件 <br>
>>config        --配置文件 <br>



#### 运行步骤
    1. debug.sh
    2. killall.sh 结束所有skynet进程

#### 规范和约定说明
    1. server结尾的文件夹代表一个节点（进程）
    2. 每个节点文件夹里一般会有个manager, 负责节点的服务管理, 跨节点消息处理
    3. _service.lua 结尾的lua文件代码一个服务起动入口
    4. 服务里的逻辑代码以类的形式存在，类文件名以大写字母开头如Logic.lua
    5. 类函数，使用小驼峰名字
    6. 变量使用小字母加_ 的形式命名, 如 message_name 尽量不要让名字太长.
    7. 集群节点间消息通信 格式为 xxx_req, xxx_res, xxx_nt 代表 请求,回复,推送
    8. 服务间消息格式为 xxx_xxx
    9. 与客户端间消息格式为 xxxReq, xxxRes, xxxNt

#### 解决的问题
    1. 如何设计分布式集群, 管理集群
    2. 如果处理日记
    3. 如何更好的处理db增删改查操作, 不用写sql语句
    4. 网关与消息转发
    5. 机器人管理设计
    6. 实现代码热更, 配置文件热更
    7. 登录用户对角管理

#### 目标

    1. 探讨skynet的一种开发方式
    2. 服务器框架的各个模块的再学习与整理
    3. 代码热更新
    4. loggerserver
    5. dbserver
    6. gateserver
    7. zoneserver
    8. gameserver
    9. hallserver
    10. 集群管理
    11.
    100. 感兴趣的朋友加qq群 373245593 一起学习

#### 各个服务器职责说明
    1. zoneserver 
        接收各个服务器基本信息
        用户与服务器对应信息
        与后面保持通信
    2. loggerserver 
        日记按服务器名与日期写入
    3. dbserver
        数据库增删改查操作
    4. nodeserver
        网关消息中转到各个服务器， 各个服务器消息中转到客户端
    5. hallserver
        大厅业务服务器
    6. gameserver
        游戏服务器
    7. robotserver
        机器人控制服务器
    8. robot
        各个游戏的机器人
    9. robottest
        用于各游戏的单元测试



#### 消息说明:

    1. 向NodeServer 注册服务器
    server_register_req
    {
        server_id           --服务器id
        server_type         --服务器类型
        server_port         --服务器对外socket port
        server_wport        --服务器对外websocket port
        server_name         --服务名
        cluster_addr        --集群地址
        cluster_id          --集群id
    }
    server_register_res
    {
        error_code = 0,
        error_msg = "服务器注册成功"
    }

    2. 向nodeserver注册收到返回后，通知zoneserver自己的服务器信息
    server_info_nt
    {
        server_id           --服务器id
        server_type         --服务器
        server_name         --服务器名
        cluster_addr        --集群地址
        cluster_id          --集群id
        max_client          --最大客户端数
        online_count        --在线数
    }   

    3. 请求分配游戏网关 loginserver -> zoneserver
    assign_server_req = {
        user_id = user_id,  --用户id
        token = token,      --token
    }
    assign_sever_result = {
        user_id = user_id,
        token = token,        
        server_id = gate_server_id,
        server_type = "gateserver",
        server_name = "游戏网关",
        server_addr = gate_server_addr,
        server_port = gate_server_port,
        server_wport = gate_server_wport,            
    }

    4. 请求分配桌子 hallserver -> zoneserver
    assign_game_server_req = {
        user_id = user_id,              --用户id
        game_id = msg_data.game_id,     --游戏id
        group_id = msg_data.group_id,   --房间id
        desk_id = msg_data.desk_id,     --桌子id
    }

    5. 通知nodeserver user_id 所关联的服务器
    assign_sever_nt = {
        user_id = user_id,
        gate_server_id = gate_server_id,  --网关服务器id          
        hall_server_id = hall_server_id,  --大厅服务器id
        update_time = update_time,        --更新时间
    }

    6. 请求游戏服务器  hallserver -> zoneserver  
    assign_game_server_req = {
        user_id = user_id,
        game_id = msg_data.game_id,     --游戏id
        group_id = msg_data.group_id,   --房间id
        desk_id = msg_data.desk_id,     --桌子id
    }
    assign_game_server_res = {
        error_code = assign_result,
        error_msg = "分配游戏服务器成功",
        user_id = user_id,
        game_id = game_id,
        group_id = group_id,
        desk_id = desk_id,
        server_id = game_server.server_id,
        server_type = game_server.server_type,
        server_name = game_server.server_name,
        cluster_addr = game_server.cluster_addr,
        gate_server_id = user_assign.gate_server_id,
    }  


